from django.contrib import admin
from django.http import JsonResponse
from django.utils.safestring import mark_safe
from django.urls import reverse
from .models import Ukuran, ModelBaju, WaktuPengerjaan, Transaksi, BiayaTambahan, PertanyaanKuesioner, Kuesioner, RiwayatTransaksi
from .forms import ModelBajuForm
from master.utils import formatrupiah

class UkuranAdmin(admin.ModelAdmin):
	list_display = ('nama_ukuran', 'keterangan')

class ModelBajuAdmin(admin.ModelAdmin):
	list_display = ('nama_model_baju', 'penjahit', 'get_ukuran', 'harga_rp', 'keterangan')
	search_fields = ('nama_model_baju', 'harga')
	# form = ModelBajuForm

	def get_ukuran(self, obj):
		ukuran = ""
		if obj.ukuran:
			ukuran = obj.list_ukuran()
		return ukuran
	get_ukuran.short_description = "Ukuran"

	def get_list_display(self, request):
		from django.urls import resolve
		list_display = self.list_display
		if request.user.is_penjahit():
			list_display = ('nama_model_baju', 'get_ukuran', 'harga_rp', 'keterangan')
		return list_display

	def get_fieldsets(self, request, obj = None):
		fieldsets = ()
		if request.user.is_superuser:
			fieldsets = (
				(None, {'fields': ('penjahit', 'ukuran', 'nama_model_baju', 'foto', 'harga', 'keterangan') }),
			)
		elif request.user.is_penjahit():
			fieldsets = (
				(None, {'fields': ('ukuran', 'nama_model_baju', 'foto', 'harga', 'keterangan') }),
			)
		return fieldsets

	def get_queryset(self, request):
		qs = super().get_queryset(request)
		if request.user.is_penjahit():
			qs = qs.filter(penjahit=request.user.penjahit)
		return qs

	def save_model(self, request, obj, form, change):
		if request.user.is_penjahit():
			obj.penjahit = request.user.penjahit
		obj.save()

class WaktuPengerjaanAdmin(admin.ModelAdmin):
	list_display = ('hari', 'keterangan')

class BiayaTambahanAdmin(admin.ModelAdmin):
	list_display = ('penjahit', 'get_hari', 'get_nominal', 'keterangan')

	def get_hari(self, obj):
		if obj.waktu_pengerjaan:
			if obj.waktu_pengerjaan.hari:
				return str(obj.waktu_pengerjaan.hari)+" Hari"
		return "-"
	get_hari.short_description = "Waktu Pengerjaan"

	def get_nominal(self, obj):
		if obj.nominal:
			return formatrupiah(obj.nominal, True)
		return "Rp. 0"
	get_nominal.short_description = "Nominal"

	def get_fieldsets(self, request, obj = None):
		fieldsets = ()
		if request.user.is_superuser:
			fieldsets = (
				(None, {'fields': ('penjahit', 'waktu_pengerjaan', 'nominal', 'keterangan') }),
			)
		elif request.user.is_penjahit():
			fieldsets = (
				(None, {'fields': ('waktu_pengerjaan', 'nominal', 'keterangan') }),
			)
		return fieldsets

	def get_list_display(self, request):
		from django.urls import resolve
		list_display = self.list_display
		if request.user.is_penjahit():
			list_display = ('get_hari', 'get_nominal', 'keterangan')
		return list_display

	def get_queryset(self, request):
		qs = super().get_queryset(request)
		if request.user.is_penjahit():
			qs = qs.filter(penjahit=request.user.penjahit)
		return qs

	def save_model(self, request, obj, form, change):
		if request.user.is_penjahit():
			obj.penjahit = request.user.penjahit
		obj.save()

class TransaksiAdmin(admin.ModelAdmin):
	list_display_links = None
	list_display = ('nomor_transaksi', 'customer', 'penjahit', 'baju', 'ukuran', 'get_harga', 'get_waktu_pengerjaan', 'keterangan', 'aksi')
	search_fields = ('nomor_transaksi', 'customer__nama_lengkap')

	def get_harga(self, obj):
		if obj.total_harga:
			return formatrupiah(obj.total_harga, True)
		return "Rp. 0"
	get_harga.short_description = "Total Harga"

	def get_waktu_pengerjaan(self, obj):
		if obj.biaya_tambahan:
			if obj.biaya_tambahan.waktu_pengerjaan:
				return str(obj.biaya_tambahan.waktu_pengerjaan.hari)+" Hari"
		return "-"
	get_waktu_pengerjaan.short_description = "Waktu Pengerjaan"

	def aksi_verifikasi(self, request):
		results = {'success': False}
		transaksi_id = request.GET.get('transaksi_id', None)
		act = request.GET.get('act', None)
		if transaksi_id and act:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				pesan = ""
				if act == 'terima':
					transaksi_obj.status = 8
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah diterima oleh Penjahit'
						)
					riwayat.save()
					pesan = "Pesanan telah diterima oleh Penjahit"
				elif act == 'selesaidijahit':
					transaksi_obj.status = 10
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah selesai dijahit'
						)
					riwayat.save()
					pesan = "Pesanan telah selesai dijahit"
				elif act == 'dikirim':
					transaksi_obj.status = 11
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah dikirim ke Customer'
						)
					riwayat.save()
					pesan = "Pesanan telah dikirim ke Customer"
				elif act == 'tolak':
					transaksi_obj.status = 13
					riwayat = RiwayatTransaksi(
						transaksi=transaksi_obj,
						nama_riwayat='Pesanan telah ditolak oleh penjahit.'
						)
					riwayat.save()
					pesan = "Pesanan telah ditolak oleh penjahit."
				transaksi_obj.save()
				results = {'success': True, 'pesan': pesan}
		return JsonResponse(results, safe=False)

	def aksi(self, obj):
		html_ = ''
		# print(obj.status)
		if obj.status == 6:
			html_ += '<button type="button" class="btn btn-primary" onclick="aksi_verifikasi($(this))" data-url="%s?transaksi_id=%s&act=terima" data-pesan="%s">Terima</button>' % (reverse('admin:transaksi__transaksi__aksi_verifikasi'), obj.id, 'Apakah Anda yakin untuk menerima transaksi ini '+obj.nomor_transaksi+' ?')
			html_ += '<button type="button" class="btn btn-danger" onclick="aksi_verifikasi($(this))" data-url="%s?transaksi_id=%s&act=tolak" data-pesan="%s">Tolak</button>' % (reverse('admin:transaksi__transaksi__aksi_verifikasi'), obj.id, 'Apakah Anda yakin untuk menolak transaksi ini '+obj.nomor_transaksi+' ?')
		elif obj.status == 9:
			# selesaidijahit
			html_ += '<button type="button" class="btn btn-primary" onclick="aksi_verifikasi($(this))" data-url="%s?transaksi_id=%s&act=selesaidijahit" data-pesan="%s">Terjahit</button>' % (reverse('admin:transaksi__transaksi__aksi_verifikasi'), obj.id, 'Apakah pesanan ini '+obj.nomor_transaksi+' sudah terjahit ?')
		elif obj.status == 10:
			# kirim
			html_ += '<button type="button" class="btn btn-primary" onclick="aksi_verifikasi($(this))" data-url="%s?transaksi_id=%s&act=dikirim" data-pesan="%s">Kirim</button>' % (reverse('admin:transaksi__transaksi__aksi_verifikasi'), obj.id, 'Apakah pesanan ini '+obj.nomor_transaksi+' sudah siap untuk dikirim ?')
		html_ += '<button type="button" class="btn btn-warning" onclick="list_modal_riwayat($(this))" data-url="%s?transaksi_id=%s">Riwayat</button>' % (reverse('admin:transaksi__riwayattransaksi__as_json'), obj.id)
		return mark_safe(html_)
	aksi.short_description = "Aksi"

	def get_list_display(self, request):
		from django.urls import resolve
		list_display = self.list_display
		if request.user.is_penjahit():
			list_display = ('nomor_transaksi', 'customer', 'baju', 'ukuran', 'get_harga', 'get_waktu_pengerjaan', 'keterangan', 'aksi')
		return list_display

	def get_queryset(self, request):
		qs = super().get_queryset(request)
		if request.user.is_penjahit():
			qs = qs.filter(penjahit=request.user.penjahit)
		return qs

	def get_urls(self):
		from django.conf.urls import url
		urls = super().get_urls()
		my_urls = [
			url(r'^verifikasi/$', self.admin_site.admin_view(self.aksi_verifikasi), name='transaksi__transaksi__aksi_verifikasi'),
			# url(r'^set-driver/$', self.admin_site.admin_view(self.transaksi_set_driver), name='transaksi__transaksi__transaksi_set_driver'),
		]
		return my_urls + urls

class RiwayatTransaksiAdmin(admin.ModelAdmin):
	list_display = ('transaksi', 'nama_riwayat', 'tanggal_riwayat', 'keterangan')

	def json_riwayat_transaksi(self, request):
		results = {'success': False}
		transaksi_id = request.GET.get('transaksi_id', None)
		if transaksi_id:
			try:
				transaksi_obj = Transaksi.objects.get(id=transaksi_id)
			except Transaksi.DoesNotExist:
				pass
			else:
				results = {'success': True, 'data': [ob.as_json() for ob in transaksi_obj.riwayattransaksi_set.all()]}
		return JsonResponse(results, safe=False)

	def get_urls(self):
		from django.conf.urls import url
		urls = super().get_urls()
		my_urls = [
			url(r'^json/$', self.admin_site.admin_view(self.json_riwayat_transaksi), name='transaksi__riwayattransaksi__as_json'),
			# url(r'^set-driver/$', self.admin_site.admin_view(self.transaksi_set_driver), name='transaksi__transaksi__transaksi_set_driver'),
		]
		return my_urls + urls

class PertanyaanKuesionerAdmin(admin.ModelAdmin):
	list_display = ('pertanyaan', 'keterangan')

class KuesionerAdmin(admin.ModelAdmin):
	list_display = ('transaksi', 'pertanyaan', 'jawaban')

admin.site.register(Ukuran, UkuranAdmin)
admin.site.register(ModelBaju, ModelBajuAdmin)
admin.site.register(WaktuPengerjaan, WaktuPengerjaanAdmin)
admin.site.register(BiayaTambahan, BiayaTambahanAdmin)
admin.site.register(Transaksi, TransaksiAdmin)
admin.site.register(RiwayatTransaksi, RiwayatTransaksiAdmin)
admin.site.register(PertanyaanKuesioner, PertanyaanKuesionerAdmin)
admin.site.register(Kuesioner, KuesionerAdmin)