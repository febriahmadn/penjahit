from django.test import TestCase
from accounts.models import Penjahit


class PenjahitCase(TestCase):
	def list_penjahit(self):
		penjahit_list = Penjahit.objects.all()
		for p in penjahit_list:
			print(p)


def list_penjahit():
	penjahit_list = Penjahit.objects.all()
	data_list = []
	for p in penjahit_list:
		data_list.append(p)


# sorting using custom key
employees = [
    {'Name': 'Alan Turing', 'jarak': 25, 'salary': 10000},
    {'Name': 'Sharon Lin', 'jarak': 30, 'salary': 8000},
    {'Name': 'John Hopkins', 'jarak': 18, 'salary': 1000},
    {'Name': 'Mikhail Tal', 'jarak': 40, 'salary': 15000},
]

# sort by name (Ascending order)
employees.sort(key=lambda x: x.get('Name'))
print(employees, end='\n\n')

# sort by Age (Ascending order)
employees.sort(key=lambda x: x.get('jarak'))
print(employees, end='\n\n')

# sort by salary (Descending order)
employees.sort(key=lambda x: x.get('salary'), reverse=True)
print(employees, end='\n\n')

