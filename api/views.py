from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.urls import reverse
# Create your views here.
@login_required
def home(request, extra_context={}):
	# from transaksi.models import Jawaban, Kuesioner
	extra_context.update({
		'title': 'Selamat Datang',
		# 'menu': None
		# 'jawaban_list': Jawaban.objects.all(),
		# 'kuesioner_list': Kuesioner.objects.all(),
		})
	return render(request, "admin/home.html", extra_context)

@login_required
def menu(request):
	results = {'success' : False}
	menus = []
	if request.user.is_penjahit():
		menus.append({'name': 'Beranda', 'icon': 'fa fa-home', 'url': '/'})
		menus.append({'name': 'Transaksi', 'icon': 'fa fa-cart-plus', 'url': reverse('admin:transaksi_transaksi_changelist')})
		menus.append({'name': 'Model Baju', 'icon': 'fa fa-address-book', 'url': reverse('admin:transaksi_modelbaju_changelist')})
		menus.append({'name': 'Biaya Tambahan', 'icon': 'fa fa-dollar', 'url': reverse('admin:transaksi_biayatambahan_changelist')})
		menus.append({'name': 'Edit Profil', 'icon': 'fa fa-user', 'url': '/accounts/penjahit/'+str(request.user.id)+'/change/'})
		results = {'success' : True, 'pesan': '.', 'menus': menus }
	return JsonResponse(results, safe=False)